# Wandr Library

Utilities for Wandr Python Modules.

These are technically not specific to Wandr LLC and could be used as open-source software.

[![coverage report](https://gitlab.com/wandr/wandrlib/badges/master/coverage.svg)](https://gitlab.com/wandr/wandrlib/commits/master)
[![pipeline status](https://gitlab.com/wandr/wandrlib/badges/master/pipeline.svg)](https://gitlab.com/wandr/wandrlib/commits/master)
