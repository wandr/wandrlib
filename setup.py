"""
Copyright:
2018 Wandr LLC; All Rights Reserved.

Description:
Utilities for Wandr modules.
"""
from setuptools import find_packages
from setuptools import setup


def readme() -> str:
    """
    Returns the README.txt file for a long description.
    :returns: Read file as a string.
    """
    with open('README.md') as readme_file:
        return str(readme_file)


setup(
    author='Russell Bunch',
    author_email='russell@wandr.us',
    dependency_links=[
        "git+https://gitlab.com/wandr/mirrors/Pyrebase.git#egg=pyrebase",
    ],
    description='Microservices for data node interactions.',
    extras_require={
        'ci': [
            'tox',
        ],
        'lint': [
            'flake8',
        ],
        'unit': [
            'coverage',
            'nose',
        ],
    },
    include_package_data=True,
    install_requires=[
        'googlemaps<2.6.0',
        'pyrebase',
    ],
    license='Apache 2.0',
    long_description=readme(),
    maintainer='Wandr Team',
    maintainer_email='russell@wandr.us',
    name='wandrlib',
    packages=find_packages(),
    test_suite='nose.collector',
    url='https://gitlab.com/wandr/wandrlib',
    version='1.0.0',
    zip_safe=False,
)
