"""
Copyright:
2018 Wandr LLC; All Rights Reserved.

Description:
Authenticated API for Google Maps and Firebase.
"""
import os
from contextlib import contextmanager

import googlemaps
import googlemaps.geocoding
import googlemaps.places
from googlemaps.distance_matrix import distance_matrix
from pyrebase import pyrebase

from wandrlib.logger import Logger

LOG = Logger(__name__)


class Interface(object):
    """
    Provides ingress and egress for server side applications and databases.
    """

    def __init__(self) -> None:
        self._gm = googlemaps.Client(key=os.environ.get('GMAPS_API_KEY', ''))
        self._pb = pyrebase.initialize_app()

    @property
    def gm(self) -> googlemaps.Client:
        """
        The Google Maps interface for interacting with Google Places and its sub-libraries.
        :returns: A Google Maps interface.
        """
        if not self._gm:
            self._gm = googlemaps.Client(key=os.environ.get('GMAPS_API_KEY', ''))
        return self._gm

    @property
    def pdb(self) -> pyrebase.Firebase:
        """
        The Pyrebase (Firebase) interface for interacting with the database.
        :returns: An unofficial Firebase wrapper.
        """
        if not self._pb:
            self._pb = pyrebase.initialize_app()
        return self._pb

    def autocomplete(self, **kwargs) -> list:
        """
        Returns places for autocomplete based on given text.
        :param kwargs: Arguments to pass along to Google Maps.
        :returns predictions: A list of found predictions.
        """
        LOG.debug('^QUERY: %s', kwargs)
        predictions = googlemaps.places.places_autocomplete(self._gm, **kwargs)
        if not predictions:
            LOG.debug(f"No place details found for {kwargs['input_text']}")
            return []
        LOG.debug(f"Found {len(predictions)} for {kwargs['input_text']}")
        return predictions

    def details(self, **kwargs) -> dict:
        """
        Retrieve details about a given place.
        :param kwargs: Arguments to pass along to Google Maps.
        :returns details: Details for the given query.
        """
        LOG.debug('^QUERY: %s', kwargs)
        details = googlemaps.places.place(self._gm, **kwargs)
        if not details:
            LOG.debug(f"No place details found for {kwargs['place_id']}")
            return {}
        LOG.debug(f"Found details for {kwargs['place_id']} : {details}")
        return details

    def geocode(self, **kwargs) -> dict:
        """
        Returns the reversed geocode for a given (lat, lng) or the geocode for a given address.
        :param kwargs: Arguments to pass along to Google Maps.
        :returns: Google results for given type of geocode request.
        """
        # TODO: Errors raised?
        if kwargs.get('latlng'):
            return googlemaps.geocoding.reverse_geocode(self._gm, **kwargs)
        elif kwargs.get('address'):
            return googlemaps.geocoding.geocode(self._gm, **kwargs)

    def matrix(self, places: list, **kwargs: dict) -> dict:
        """
        Maps out a matrix of time and distance based on this node as an Origin.
        :param places: List of places to create a mapping for.
        :param kwargs: Arguments to pass along to Google Maps.
        :returns matrix: Dictionary of rows denoting distance and time.
        """

        def _location(place: dict) -> list:
            """Simple dictionary extractor."""
            location_set = place['geometry']['location']
            return [location_set['lat'], location_set['lng']]

        matrix = {'status': False}
        if not places:
            LOG.debug('No matrix for place-less call.')
            return matrix
        locations = [_location(place) for place in places]
        args = [self._gm, locations[:-1], locations[1:]]
        LOG.debug('^MATRIX QUERY: %s (options: %s)', args, kwargs)
        try:
            matrix = distance_matrix(*args, **kwargs)
        except googlemaps.exceptions.HTTPError as error:
            LOG.error('Communication error for distance matrix: %s', error)
        except googlemaps.exceptions.ApiError as error:
            LOG.error('Malformed distance matrix request: %s', error)
        return matrix

    def nearby(self, place_types: list, **kwargs: dict) -> list:
        """
        Gets everything nearby the origin.
        :param place_types: List of placeTypes, each will be searched.
        :param kwargs: Arguments to pass along to Google Maps.
        :returns places: List of places, organized by type
        """
        places = []
        for place_type in place_types:
            place_kwargs = kwargs.copy()
            place_kwargs['type'] = place_type
            LOG.debug('^QUERY: %s', place_kwargs)
            found = googlemaps.places.places_nearby(self._gm, **place_kwargs)
            if found['status'] == 'OK':
                LOG.info('Using 1, throwing %i', len(found['results']) - 1)
                places.append(found['results'][0])
            elif found['status'] == 'ZERO_RESULTS':
                LOG.debug('No results for request %s', place_kwargs)
            else:
                LOG.debug('Bad status on request %s', place_kwargs)
        return places


@contextmanager
def connection() -> Interface:
    """
    Creates a clean connection to backend services.
    :returns: A connection to use.
    """
    api = Interface()
    try:
        yield api
    except ValueError as error:
        LOG.error('Invalid credentials - %s', error)
    except NotImplementedError as error:
        LOG.error('Could not connect to Google.', error)
    finally:
        del api
