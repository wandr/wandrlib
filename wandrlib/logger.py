"""
Copyright:
2018 Wandr LLC; All Rights Reserved.

Description:
General use logger.
"""

import logging
import sys


class Logger(logging.Logger):
    def __init__(self, module_name: str, level: int = logging.DEBUG) -> None:
        """
        Initializes a new Logger with module_name. Ideally, the module_name
        is the __name__ variable. The level will default to logging.INFO.
        :param module_name:
        :param level:
        """
        super(Logger, self).__init__(module_name, level)
        formatter = logging.Formatter('%(asctime)s %(levelname)-8s | %(name)-24s | %(message)s')
        formatter.datefmt = '%b %d %H:%M:%S'
        console = logging.StreamHandler(sys.stdout)
        console.setFormatter(formatter)
        console.setLevel(level)
        self.addHandler(console)

    def isDebug(self) -> bool:
        """
        Simple wrapper to return whether or not this is set to logger.DEBUG.
        :returns: True if debug, false otherwise.
        """
        return self.isEnabledFor(logging.DEBUG)
