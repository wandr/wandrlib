"""
Copyright:
2018 Wandr LLC; All Rights Reserved.
"""
import unittest

from wandrlib.util import g_normalize


class UtilTestCase(unittest.TestCase):
    def test_normalize_keys(self):
        """Assert we get a normalized set of keys from a given query."""
        expected = {
            'place_id': 'foo',
            'strict_bounds': 'foo',
            'input_text': 'foo',
        }
        test_query = {
            'placeid': 'foo',
            'strictbounds': 'foo',
            'input': 'foo',
        }
        actual = g_normalize(test_query)
        self.assertEqual(expected, actual)

    def test_normalize_vars(self):
        """Assert we get a normalized set of vars from a given query."""
        expected = {
            'latlng': ['foo', 'foo'],
        }
        test_query = {
            'latlng': 'foo,foo',
        }
        actual = g_normalize(test_query)
        self.assertEqual(expected, actual)

    def test_removes_api_keys(self):
        """Assert that API keys are filtered out of queries."""
        expected = {
            'foo': 'bar',
        }
        test_query = {
            'foo': 'bar',
            'key': 'forbidden',
        }
        actual = g_normalize(test_query)
        self.assertEqual(expected, actual)
