"""
Copyright:
2018 Wandr LLC; All Rights Reserved.

Description:
Utility methods.
"""

KEY_LEX = {
    'placeid': 'place_id',
    'strictbounds': 'strict_bounds',
    'input': 'input_text',
}
VAR_LEX = {
    'latlng': ',',
}


def g_normalize(query: dict) -> dict:
    """
    Converts parameters from Google requests to SDK compatible requests for special cases. These
    cases can be anything, such as the JS variable `placeid` being labeled as `place_id` instead.
    This is an 'ease-of-use' function that cleans up calls.

    This also filters out API keys that were included in the query.
    :param query: A dictionary of parameters assumed to be from a Google API request.
    :returns: A dictionary with compatible/fixed variables for the Google Python SDK.
    """
    normalized = {}
    for key, value in query.items():
        if key == 'key':
            continue
        if key in KEY_LEX:
            normalized[KEY_LEX[key]] = value
        elif key in VAR_LEX:
            normalized[key] = value.split(VAR_LEX[key])
        else:
            normalized[key] = value
    return normalized
